package az.ingress.assignments;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class AssignmentsMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(AssignmentsMsApplication.class, args);
    }

}
