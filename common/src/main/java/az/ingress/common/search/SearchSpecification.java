package az.ingress.common.search;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class SearchSpecification<T> implements Specification<T> {

    private static final long serialVersionUID = 3522416053866116034L;

    private final List<SearchCriteria> criteriaList;
    private final MatchType matchType;

    public SearchSpecification(List<SearchCriteria> criteriaList) {
        this.criteriaList = new ArrayList<>(criteriaList);
        this.matchType = MatchType.AND;
    }

    public SearchSpecification(List<SearchCriteria> criteriaList, MatchType matchType) {
        this.criteriaList = new ArrayList<>(criteriaList);
        this.matchType = matchType;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Predicate[] predicates = criteriaList.stream()
                .map(criteria -> criteria.getOperation().buildPredicate(root, criteria, builder))
                .toArray(Predicate[]::new);
        return matchType == MatchType.OR ? builder.or(predicates) : builder.and(predicates);
    }
}
