package az.ingress.assignments.service.impl;

import az.ingress.assignments.domain.Job;
import az.ingress.assignments.dto.JobRequestDto;
import az.ingress.assignments.dto.JobResponseDto;
import az.ingress.assignments.errors.Errors;
import az.ingress.assignments.repository.JobRepository;
import az.ingress.assignments.service.JobService;
import az.ingress.common.exception.ApplicationException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class JobServiceImpl implements JobService {

    private final ModelMapper mapper;
    private final JobRepository jobRepository;

    @Override
    public void create(JobRequestDto dto, String username) {
        checkIfJobIsUnique(dto.getName());
        Job job = Job.builder()
                .name(dto.getName())
                .createdBy(1l)
                .build();
        jobRepository.save(job);
    }

    @Override
    public JobResponseDto update(Long jobId, JobRequestDto dto, String username) {
        checkIfJobIsUnique(dto.getName(), jobId);
        Job job = jobRepository.findById(jobId)
                .orElseThrow(() -> new ApplicationException(Errors.JOB_NOT_FOUND));
        job.setName(dto.getName());
        job.setLastUpdatedBy(1l);
        job.setLastUpdateDate(LocalDateTime.now());
        jobRepository.save(job);
        return mapper.map(job, JobResponseDto.class);
    }

    @Override
    public void delete(Long jobId) {
        jobRepository.delete(jobRepository.findById(jobId)
                .orElseThrow(() -> new ApplicationException(Errors.JOB_NOT_FOUND)));
    }

    @Override
    public List<JobResponseDto> getAll() {
        return jobRepository.findAll().stream()
                .map(job -> mapper.map(job, JobResponseDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public Page<JobResponseDto> search(String name, Pageable pageable) {
//        JobSpecification jobSpecification = new JobSpecification();
//
//        jobSpecification.add(SearchCriteria.builder()
//                .key("name")
//                .value(name.toLowerCase())
//                .operation(SearchOperation.MATCH)
//                .build());

//        return jobRepository.findAll(jobSpecification, pageable)
//                .map(job -> mapper.map(job, JobResponseDto.class));
        return null;
    }

    private void checkIfJobIsUnique(String name) {
        jobRepository.findByName(name)
                .ifPresent(job -> {
                    throw new ApplicationException((Errors.JOB_ALREADY_EXISTS),
                            Map.of("name", name));});
    }

    private void checkIfJobIsUnique(String name, Long jobId) {
        jobRepository.findByNameAndId(name, jobId)
                .ifPresent(job -> {
                    throw new ApplicationException((Errors.JOB_ALREADY_EXISTS),
                            Map.of("name", name));});
    }

}
