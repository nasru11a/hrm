package az.ingress.common.search;

public enum MatchType {
    AND, OR
}
