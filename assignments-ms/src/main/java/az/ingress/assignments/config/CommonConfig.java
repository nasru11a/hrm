package az.ingress.assignments.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"az.ingress.common.config"})
public class CommonConfig {

}
