package az.ingress.assignments.service;

import az.ingress.assignments.dto.JobRequestDto;
import az.ingress.assignments.dto.JobResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface JobService {

    void create(JobRequestDto dto, String name);

    JobResponseDto update(Long jobId, JobRequestDto dto, String username);

    void delete(Long jobId);

    List<JobResponseDto> getAll();

    Page<JobResponseDto> search(String name, Pageable pageable);
}
