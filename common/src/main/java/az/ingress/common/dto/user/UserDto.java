package az.ingress.common.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.TooManyFields")
public class UserDto {

    private Long id;

    @NotNull
    private String about;
    private String phone;

    @Email
    @NotNull
    private String username;
    private String organisation;
    private String settings;
    private String avatar;
    private String name;
    private String title;
    private String background;
    private String company;
    private String address;
    private LocalDate birthday;
    private Set<AuthorityDto> authorities;
    private Object[] tags;
    private Object[] phoneNumbers;
    private Object[] emails;
    private Boolean enabled;
    private Boolean accountNonExpired;
    private Boolean accountNonLocked;
    private Boolean credentialsNonExpired;

}

