package az.ingress.user.management.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignInDto {

    @Email
    @NotBlank(message = "Email should not be blank.")
    private String email;

    @NotBlank(message = "Password should not be empty.")
    private String password;
}
