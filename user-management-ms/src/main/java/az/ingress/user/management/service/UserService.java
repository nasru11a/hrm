package az.ingress.user.management.service;

import az.ingress.user.management.dto.JwtResponseDto;
import az.ingress.user.management.dto.SignInDto;
import az.ingress.user.management.dto.SignUpDto;

public interface UserService {
    void signUp(SignUpDto dto);

    JwtResponseDto signIn(SignInDto dto);
}
