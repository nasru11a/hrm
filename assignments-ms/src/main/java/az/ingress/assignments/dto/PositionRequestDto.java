package az.ingress.assignments.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PositionRequestDto {
    @NotNull(message = "Structure is required.")
    private Long structureId;

    @NotNull(message = "Job is required.")
    private Long jobId;

    @Size(min = 200, message = "Salary is required. Minimum salary is 200.")
    private Integer salary;

    @NotNull(message = "Grade is required.")
    private String grade;

}
