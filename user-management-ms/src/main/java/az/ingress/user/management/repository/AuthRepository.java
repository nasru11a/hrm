package az.ingress.user.management.repository;

import az.ingress.user.management.domain.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthRepository extends JpaRepository<Authority, Long> {
    Optional<Authority> findByAuthority(String authority);
}
