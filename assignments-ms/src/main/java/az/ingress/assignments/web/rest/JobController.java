package az.ingress.assignments.web.rest;

import az.ingress.assignments.dto.JobRequestDto;
import az.ingress.assignments.dto.JobResponseDto;
import az.ingress.assignments.service.JobService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/assignment/job")
public class JobController {

    private final JobService jobService;

    @PostMapping("/create")
    public void createJob(@RequestBody @Valid JobRequestDto dto, Principal principal) {
        log.info("Create job request by user {}", principal.getName());
        jobService.create(dto, principal.getName());
    }

    @PutMapping("/update")
    public ResponseEntity<JobResponseDto> update(@RequestParam Long jobId, @RequestBody @Valid JobRequestDto dto, Principal principal) {
        log.info("Update job request by user {}", principal.getName());
        return ResponseEntity.ok(jobService.update(jobId, dto, principal.getName()));
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam Long jobId, Principal principal) {
        log.info("Delete job request by user {}", principal.getName());
        jobService.delete(jobId);
    }

    @GetMapping("/get-all")
    public List<JobResponseDto> getAll(Principal principal) {
        log.info("Get all jobs request by user {}", principal.getName());
        return jobService.getAll();
    }

    @GetMapping("/search")
    public ResponseEntity<Page<JobResponseDto>> search(@RequestParam(required = false, defaultValue = "") String name,
                                                       Pageable pageable,
                                                       Principal principal) {
        log.info("Search job request with user {}", principal.getName());
        return ResponseEntity.ok(jobService.search(name, pageable));
    }

}
