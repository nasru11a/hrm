package az.ingress.user.management.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignUpDto {

    @Email(message = "Invalid email format.")
    @NotBlank(message = "Email should not be blank.")
    private String email;

    @NotBlank(message = "Password should not be empty.")
    private String password;

    @NotBlank(message = "Firstname should not be blank.")
    private String firstName;

    private String lastName;

    @NotBlank(message = "Phone number is required.")
    private String phone;

    private String company;

    private LocalDate birthday;

    private String organisation;

}
