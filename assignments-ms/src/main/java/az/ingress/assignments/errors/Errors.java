package az.ingress.assignments.errors;

import az.ingress.common.exception.ErrorResponse;
import org.springframework.http.HttpStatus;

public enum Errors implements ErrorResponse {

    EMAIL_ALREADY_REGISTERED("EMAIL_ALREADY_REGISTERED", HttpStatus.BAD_REQUEST,
            "Given email {email} is already registered"),
    USER_NOT_FOUND("USER_NOT_FOUND", HttpStatus.BAD_REQUEST,
            "User not found in the system."),
    INVALID_PASSWORD("INVALID_PASSWORD", HttpStatus.BAD_REQUEST,
            "Password is invalid."),
    AUTHORITY_NOT_FOUND("AUTHORITY_NOT_FOUND", HttpStatus.BAD_REQUEST,
            "Such authority not found in the system."),
    JOB_ALREADY_EXISTS("JOB_ALREADY_EXISTS", HttpStatus.BAD_REQUEST,
            "Given job with name \"{name}\" is already exists."),
    JOB_NOT_FOUND("JOB_NOT_FOUND", HttpStatus.BAD_REQUEST,
            "Given job is not found in the system."),
    STRUCTURE_ALREADY_EXISTS("STRUCTURE_ALREADY_EXISTS", HttpStatus.BAD_REQUEST,
            "Given structure is already exists in the system."),
    PARENT_STRUCTURE_NOT_FOUND("PARENT_STRUCTURE_NOT_FOUND", HttpStatus.BAD_REQUEST,
            "Given parent structure is not found in the system."),
    UNSUPPORTED_PARENT_CHILD_STRUCTURE_COMBINATION("UNSUPPORTED_PARENT_CHILD_STRUCTURE_COMBINATION(", HttpStatus.BAD_REQUEST,
            "Given combination between parent and child structure is not logically appropriate."),
    STRUCTURE_NOT_FOUND("STRUCTURE_NOT_FOUND", HttpStatus.BAD_REQUEST,
            "Given structure is not found in the system."),
    STRUCTURE_HAS_CHILD_STRUCTURE("STRUCTURE_HAS_CHILD_STRUCTURE", HttpStatus.BAD_REQUEST,
            "Given structure has bound with child structure. Delete the child structure first."),
    GRADE_ALREADY_EXISTS("GRADE_ALREADY_EXISTS", HttpStatus.BAD_REQUEST,
            "Given grade is already exists."),
    GRADE_NOT_FOUND("GRADE_NOT_FOUND", HttpStatus.BAD_REQUEST,
            "Grade is not found in the system.");

    String key;
    HttpStatus httpStatus;
    String message;

    Errors(String key, HttpStatus httpStatus, String message) {
        this.message = message;
        this.key = key;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
