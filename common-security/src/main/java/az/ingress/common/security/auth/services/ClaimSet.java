package az.ingress.common.security.auth.services;

import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimSet {

    private String key;
    private Set<String> claims;
}
