package az.ingress.common.security.auth.services;

import az.ingress.common.security.config.SecurityProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.Key;
import java.time.Instant;
import java.util.Date;
import java.util.Map;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public final class JwtService {
    private Key key;

    @Value("${security.jwtProperties.secret}")
    private String secret;

    @Value("${security.jwtProperties.access-token-validity-in-seconds}")
    private Long accessTokenDuration;

    @Value("${security.jwtProperties.refresh-token-validity-in-seconds}")
    private Long refreshTokenDuration;

    private final Set<ClaimSetProvider> claimSetProviders;
    private final Set<ClaimProvider> claimProviders;

    private final SecurityProperties applicationProperties;

    @PostConstruct
    public void init() {
        byte[] keyBytes =Decoders.BASE64.decode(secret);
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String issueAccessToken(Authentication authentication) {
        log.trace("Issue JWT access token to {} for {} seconds", authentication, accessTokenDuration);
        final JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(authentication.getName())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plusSeconds(accessTokenDuration)))
                .setHeader(Map.of("type", "JWT"))
                .signWith(key, SignatureAlgorithm.HS512);

        addClaimsSets(jwtBuilder, authentication);
        addClaims(jwtBuilder, authentication);
        return jwtBuilder.compact();
    }

    public String issueRefreshToken(Authentication authentication) {
        log.trace("Issue JWT refresh token to {} for {} seconds", authentication, refreshTokenDuration);
        final JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(authentication.getName())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plusSeconds(refreshTokenDuration)))
                .setHeader(Map.of("type", "JWT"))
                .signWith(key, SignatureAlgorithm.HS512);

        addClaimsSets(jwtBuilder, authentication);
        addClaims(jwtBuilder, authentication);
        return jwtBuilder.compact();
    }

    private void addClaimsSets(JwtBuilder jwtBuilder, Authentication authentication) {
        claimSetProviders.forEach(claimSetProvider -> {
            final ClaimSet claimSet = claimSetProvider.provide(authentication);
            log.trace("Adding claim {}", claimSet);
            jwtBuilder.claim(claimSet.getKey(), claimSet.getClaims());
        });
    }

    private void addClaims(JwtBuilder jwtBuilder, Authentication authentication) {
        claimProviders.forEach(claimSetProvider -> {
            final Claim claim = claimSetProvider.provide(authentication);
            log.trace("Adding claim {}", claim);
            jwtBuilder.claim(claim.getKey(), claim.getClaim());
        });
    }

}
