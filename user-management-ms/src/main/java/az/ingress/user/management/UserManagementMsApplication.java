package az.ingress.user.management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class UserManagementMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserManagementMsApplication.class, args);
    }

}
