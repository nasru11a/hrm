package az.ingress.user.management.web.rest;

import az.ingress.user.management.dto.JwtResponseDto;
import az.ingress.user.management.dto.SignInDto;
import az.ingress.user.management.dto.SignUpDto;
import az.ingress.user.management.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthController {
    private final UserService userService;

    @PostMapping("/sign-up")
    public void signUp(@RequestBody @Valid SignUpDto dto){
        log.info("Sign up request with email {}", dto.getEmail());
        userService.signUp(dto);
    }

    @PostMapping("/sign-in")
    public ResponseEntity<JwtResponseDto> signIn(@RequestBody @Valid SignInDto dto) {
        log.info("Sign ip request with email {}", dto.getEmail());
        userService.signIn(dto);
        return ResponseEntity.status(200).body(userService.signIn(dto));
    }
}
