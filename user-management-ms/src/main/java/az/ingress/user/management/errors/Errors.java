package az.ingress.user.management.errors;

import az.ingress.common.exception.ErrorResponse;
import org.springframework.http.HttpStatus;

public enum Errors implements ErrorResponse {

    EMAIL_ALREADY_REGISTERED("EMAIL_ALREADY_REGISTERED", HttpStatus.BAD_REQUEST,
            "Given email {email} is already registered"),
    USER_NOT_FOUND("USER_NOT_FOUND", HttpStatus.BAD_REQUEST,
            "User not found in the system."),
    INVALID_PASSWORD("INVALID_PASSWORD", HttpStatus.BAD_REQUEST,
            "Password is invalid."),
    AUTHORITY_NOT_FOUND("AUTHORITY_NOT_FOUND", HttpStatus.BAD_REQUEST,
            "Such authority not found in the system.");

    String key;
    HttpStatus httpStatus;
    String message;

    Errors(String key, HttpStatus httpStatus, String message) {
        this.message = message;
        this.key = key;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
