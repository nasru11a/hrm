package az.ingress.user.management.domain.enumaration;

public enum UserAuthorities {
    ROLE_CUSTOMER, ROLE_MANAGER, ROLE_USER, ROLE_ADMIN, ROLE_SUPER_USER
}
