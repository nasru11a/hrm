package az.ingress.assignments.repository;

import az.ingress.assignments.domain.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JobRepository extends JpaRepository<Job, Long>, JpaSpecificationExecutor<Job> {
    Optional<Job> findByName(String name);

    @Query("select j from Job j where j.name =:name and j.id !=:id")
    Optional<Job> findByNameAndId(@Param("name") String name, @Param("id") Long id);
}
