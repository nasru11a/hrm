package az.ingress.user.management.service.impl;

import az.ingress.common.exception.ApplicationException;
import az.ingress.common.security.auth.services.JwtService;
import az.ingress.user.management.domain.Authority;
import az.ingress.user.management.domain.User;
import az.ingress.user.management.domain.enumaration.UserAuthorities;
import az.ingress.user.management.dto.JwtResponseDto;
import az.ingress.user.management.dto.SignInDto;
import az.ingress.user.management.dto.SignUpDto;
import az.ingress.user.management.errors.Errors;
import az.ingress.user.management.repository.AuthRepository;
import az.ingress.user.management.repository.UserRepository;
import az.ingress.user.management.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final AuthRepository authRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;

    @Override
    @Transactional
    public void signUp(SignUpDto dto) {
        userRepository.findByUsername(dto.getEmail())
                .ifPresent(user -> {
                    throw new ApplicationException(Errors.EMAIL_ALREADY_REGISTERED,
                            Map.of("email", dto.getEmail()));});

        User user = createUserEntityObject(dto);
        assignAuthoritiesToUser(user, dto);
        userRepository.save(user);
    }

    @Override
    public JwtResponseDto signIn(SignInDto dto) {
        User user = userRepository.findByUsername(dto.getEmail())
                .orElseThrow(() -> new ApplicationException(Errors.USER_NOT_FOUND));
        matchPasswords(user, dto);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String accessToken = jwtService.issueAccessToken(authentication);
        String refreshToken = jwtService.issueRefreshToken(authentication);

        return JwtResponseDto.builder()
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .build();
    }

    private void matchPasswords(User user, SignInDto dto){
        boolean isMatched = passwordEncoder.matches(dto.getPassword(), user.getPassword());
        if(!isMatched) throw new ApplicationException(Errors.INVALID_PASSWORD);
    }

    private void assignAuthoritiesToUser(User user, SignUpDto dto) {
        Set<Authority> authorities = new HashSet<>();
        authorities.add(authRepository.findByAuthority(UserAuthorities.ROLE_CUSTOMER.name())
                .orElseThrow(()-> new ApplicationException(Errors.AUTHORITY_NOT_FOUND)));
        user.setAuthorities(authorities);
    }

    private User createUserEntityObject(SignUpDto dto) {
        return User.builder()
                .username(dto.getEmail())
                .password(passwordEncoder.encode(dto.getPassword()))
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .birthday(dto.getBirthday())
                .company(dto.getCompany())
                .organisation(dto.getOrganisation())
                .phone(dto.getPhone())
                .email(dto.getEmail())
                .creationDate(LocalDateTime.now())
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .build();
    }
}
