package az.ingress.user.management.config;

import az.ingress.common.security.auth.services.AuthService;
import az.ingress.common.security.config.BaseSecurityConfig;
import az.ingress.common.security.config.SecurityProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@Slf4j
@EnableWebSecurity
@ComponentScan("az.ingress.common.security")
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends BaseSecurityConfig {

    public SecurityConfiguration(SecurityProperties securityProperties, List<AuthService> authServices) {
        super(securityProperties, authServices);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/auth/sign-in",
                        "/auth/sign-up",
                        "/auth/forgot-password",
                        "/auth/activate-account",
                        "/authenticate/{socialAccountType}/{uid}",
                        "/user/{socialAccountType}/{uid}",
                        "/user/password/forgot",
                        "/user/password/reset",
                        "/user/activation/**"
                )
                .permitAll()
                .and()
                .authorizeRequests();
        super.configure(http);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
